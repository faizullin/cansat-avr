/*
 * CanSat.c
 *
 * Created: 28.05.2014 21:49:12
 * Author: Roman
 */ 

#define F_CPU 8000000UL    
#define height_change 2
#define max_impulse 1800

#include <avr/io.h>
#include <util/delay.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <stdbool.h>

#include "i2cmaster.h"
#include "adxl345.h"
#include "ms5611.h"
#include "hmc5883l.h"
#include "ff.h"
#include "diskio.h"
#include "global.h"


//Data
volatile static uint8_t pos = 0;

void uart1_init()
{
	UBRR1H = 0;
	UBRR1L = 12;
	
	UCSR1B |= (1<<TXEN1);
	UCSR1C |= (3<<UCSZ10);
	
}

ISR(USART1_UDRE_vect)
{
	if (pos<data_buffer_length) {
		UDR1=data_buf[pos++];
	}
	else {
		UCSR1B &= ~_BV(UDRIE1);
		pos = 0; 
	}
}

//Timer0. Controls sensors data.

void timer0_init()
{
	TCCR0 |= (0<<COM00)|(0<<COM01)|(1<<WGM01); //CTC mode
	TCCR0 |= (1<<CS02)|(1<<CS01)|(1<<CS00); //1024 prescaler
	TIMSK |= (1<<OCIE0); //enable interrupts
	OCR0 = 254; //interrupt every ~0.02s
}

uint8_t ovf_counter;

ISR(TIMER0_COMP_vect){
	if (++ovf_counter==3){
	ovf_counter=0;
	//disk_timerproc();
	TCNT0 = 0;	
	adxl345_updatedata(&data_buf[10]);
	hmc5883l_updatedata(&data_buf[16]);
	UCSR1B |= (1<<UDRIE1); //enable udr empty interrupt
	}
}


//Timer 3. Controls impeller state
double height,heightpr,p0;
bool going_down = false;
bool impeller_permission = false;
uint16_t impeller_force = 1000;


void timer3_init()
{
	TCCR3A = 0;
	SFIOR = 0;
	ETIFR = 0;
	TCCR3A = 0;
	OCR3A = 18750;
	TCCR3B = (1<<WGM32); //CTC mode
	TCCR3B |= (1<<CS31)|(1<<CS30); //64 prescaler
	ETIMSK |= (1<<OCIE3A);
	// configured to 150 ms interrupt
}

ISR(TIMER3_COMPA_vect)
{	
	TCNT3 = 0;
	/*if ((impeller_permission == true)&&(heightpr-height>height_change)&&(height<200)) {
		if (impeller_force<max_impulse) impeller_force += 60;
	}
	else impeller_force = 1000;
	OCR1A = impeller_force;
	if (height>100){ //чтобы не сработал на земле
		if (heightpr-height>height_change) data_buf[11]=0xEA;
		else data_buf[11]=0xAE;
	}
	heightpr = height;*/
	
	if (height
}

void debug()
{
	while (!(UCSR1A & (1<<UDRE1)));
	UDR1 = 'a';
}

int main() {
	data_buf[0]=0xA5;
	data_buf[1]=0x5A;
	
	_delay_ms(1000);
	
	uart1_init();
	i2c_init();
	timer0_init();
	timer3_init();
	//sim908_powerOn( );
	//_delay_ms(5000);
	//sim908_config();
	debug();
	adxl345_init();
	hmc5883l_init();
	_delay_ms(100);
	
	
	sei();

	while(1) {
		//sim908_getGPS();
		_delay_ms(1000);
		ms5611_requestTemperature();
		_delay_ms(10);
		ms5611_updateRawTemperature();
		ms5611_calculateTemperature();
		ms5611_temperatureToBuffer(&data_buf[2]);
		
		ms5611_requestPressure();
		_delay_ms(10);
		ms5611_updateRawPressure();
		ms5611_calculatePressure();
		ms5611_pressureToBuffer(&data_buf[6]);
		
		_delay_ms(100);
		height = ((pow((p0 / (double) pressure.num32), 0.1902256) - 1.0) * (temperature.num32 + 273.15)) / 0.0065;
	}
}