/*
 * adxl345.c
 *
 * Created: 02.06.2014 9:51:07
 * Author: Roman
 */ 

#include "adxl345.h"
#include <util/delay.h>
#include "global.h"

void adxl345_init()
{
	i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
	uint8_t range = ADXL345_RANGE | (ADXL345_FULLRANGE<<3);
	i2c_write(0x31);
	i2c_write(range);
	//power register
	i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
	i2c_write(0x2D);
	i2c_write(0x0); //disable
	i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
	i2c_write(0x2D);
	i2c_write(0x16); //stand by
	i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
	i2c_write(0x2D);
	i2c_write(0x08); //enable
	//interrupt
	i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
	i2c_write(0x2E);
	i2c_write(0x80); //data_ready on int2

	_delay_ms(20);
	i2c_stop();
}


void adxl345_updatedata(uint8_t *buffer)
{	
	//X
	i2c_start_wait(ADXL345_ADDR | I2C_WRITE);
	i2c_write(0x32);
	i2c_rep_start(ADXL345_ADDR | I2C_READ);
	*buffer = i2c_readNak();
	i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
	i2c_write(0x32+1);
	i2c_rep_start(ADXL345_ADDR | I2C_READ);
	*(buffer + 1) = i2c_readNak();
	//Y
	i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
	i2c_write(0x34);
	i2c_rep_start(ADXL345_ADDR | I2C_READ);
	*(buffer + 2) = i2c_readNak();
	i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
	i2c_write(0x34+1);
	i2c_rep_start(ADXL345_ADDR | I2C_READ);
	*(buffer + 3) = i2c_readNak();
	//Z
	i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
	i2c_write(0x36);
	i2c_rep_start(ADXL345_ADDR | I2C_READ);
	*(buffer + 4) = i2c_readNak();
	i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
	i2c_write(0x36+1);
	i2c_rep_start(ADXL345_ADDR | I2C_READ);
	*(buffer + 5) = i2c_readNak();

	i2c_stop();
}

/*uint8_t adxl345_counter = 0;
int16_t x[4];
int16_t y[4];
int16_t z[4];

void average(int16_t *buf,uint8_t *data_buf)
{
	int16_t value = (*buf + *(buf+1) + *(buf + 2) + *(buf+3))/4;
	*data_buf = value & 0xFF;
	*(data_buf + 1) = ((value & 0xFF00) >> 8);
}

void adxl345_updatedata(uint8_t *buffer)
{
	if (adxl345_counter>3) adxl345_counter=0; 
		i2c_start_wait(ADXL345_ADDR | I2C_WRITE);
		i2c_write(0x32);
		i2c_rep_start(ADXL345_ADDR | I2C_READ);
		x[adxl345_counter] = i2c_readNak();
		i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
		i2c_write(0x32+1);
		i2c_rep_start(ADXL345_ADDR | I2C_READ);
		x[adxl345_counter] |= (i2c_readNak()<<8);
		//Y
		i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
		i2c_write(0x34);
		i2c_rep_start(ADXL345_ADDR | I2C_READ);
		y[adxl345_counter] = i2c_readNak();
		i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
		i2c_write(0x34+1);
		i2c_rep_start(ADXL345_ADDR | I2C_READ);
		y[adxl345_counter] |= (i2c_readNak()<<8);
		//Z
		i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
		i2c_write(0x36);
		i2c_rep_start(ADXL345_ADDR | I2C_READ);
		z[adxl345_counter] = i2c_readNak();
		i2c_rep_start(ADXL345_ADDR | I2C_WRITE);
		i2c_write(0x36+1);
		i2c_rep_start(ADXL345_ADDR | I2C_READ);
		z[adxl345_counter] |= (i2c_readNak()<<8);

		i2c_stop();
		
		adxl345_counter++;
		average(*x,data_buf[11]);
		average(*y,data_buf[13]);
		average(*z,data_buf[15]);
}*/