/*
 * adxl345.h
 *
 * Created: 02.06.2014 9:46:43
 * Author: Roman
 */ 


#ifndef ADXL345_H_
#define ADXL345_H_

#include <avr/io.h>
#include "i2cmaster.h"

#define ADXL345_ADDR (0x53<<1) //device address
#define ADXL345_RANGE2G 0x00 //sensitivity to 2g and measurement mode
#define ADXL345_RANGE4G 0x01 //sensitivity to 4g and measurement mode
#define ADXL345_RANGE8G 0x02 //sensitivity to 8g and measurement mode
#define ADXL345_RANGE16G 0x03 //sensitivity to 16g and measurement mode
#define ADXL345_FULLRANGE 0 //1 to enable 0 to disable

#define ADXL345_RANGE ADXL345_RANGE2G

void adxl345_init();
void adxl345_updatedata(uint8_t *buffer);


#endif /* ADXL345_H_ */