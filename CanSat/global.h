/*
 * global.h
 *
 * Created: 02.07.2014 18:05:33
 * Author: Roman
 */ 


#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <avr/io.h>

#define data_buffer_length 30

extern uint8_t data_buf[data_buffer_length];



#endif /* GLOBAL_H_ */