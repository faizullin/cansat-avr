/*
 * hmc5883l.c
 *
 * Created: 02.06.2014 11:16:35
 * Author: Roman
 */ 

#include "hmc5883l.h"

static double hmc5883l_scale = 0;

void hmc5883l_init()
{
	hmc5883l_scale = 0;
	uint8_t regValue = 0x00;
	#if HMC5883L_SCALE == HMC5883L_SCALE088
	regValue = 0x00;
	hmc5883l_scale = 0.73;
	#elif HMC5883L_SCALE == HMC5883L_SCALE13
	regValue = 0x01;
	hmc5883l_scale = 0.92;
	#elif HMC5883L_SCALE == HMC5883L_SCALE19
	regValue = 0x02;
	hmc5883l_scale = 1.22;
	#elif HMC5883L_SCALE == HMC5883L_SCALE25
	regValue = 0x03;
	hmc5883l_scale = 1.52;
	#elif HMC5883L_SCALE == HMC5883L_SCALE40
	regValue = 0x04;
	hmc5883l_scale = 2.27;
	#elif HMC5883L_SCALE == HMC5883L_SCALE47
	regValue = 0x05;
	hmc5883l_scale = 2.56;
	#elif HMC5883L_SCALE == HMC5883L_SCALE56
	regValue = 0x06;
	hmc5883l_scale = 3.03;
	#elif HMC5883L_SCALE == HMC5883L_SCALE81
	regValue = 0x07;
	hmc5883l_scale = 4.35;
	#endif

	//setting is in the top 3 bits of the register.
	regValue = regValue << 5;
	i2c_start_wait(HMC5883L_ADDR | I2C_WRITE);
	i2c_write(HMC5883L_CONFREGB);
	i2c_write(regValue);
	i2c_stop();

	//set measurement mode
	i2c_start_wait(HMC5883L_ADDR | I2C_WRITE);
	i2c_write(HMC5883L_MODEREG);
	i2c_write(HMC5883L_MEASUREMODE);
	i2c_stop();
}

void hmc5883l_updatedata(uint8_t *buf)
{
	i2c_start_wait(HMC5883L_ADDR | I2C_WRITE);
	i2c_write(HMC5883L_DATAREGBEGIN);
	i2c_stop();
	i2c_start_wait(HMC5883L_ADDR | I2C_READ);
	
	*(buf + 1) = i2c_readAck();
	*buf |= i2c_readAck();
	
	*(buf + 3) = i2c_readAck();
	*(buf + 2) = i2c_readAck();
	
	*(buf + 5) = i2c_readAck();
	*(buf + 4) = i2c_readNak();
	
	i2c_stop();
}