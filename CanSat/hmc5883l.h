/*
 * hmc5883l.h
 *
 * Created: 02.06.2014 11:07:47
 * Author: Roman
 */ 


#ifndef HMC5883L_H_
#define HMC5883L_H_

#include <avr/io.h>
#include "i2cmaster.h"

#define HMC5883L_ADDR (0x1E<<1)

//registers
#define HMC5883L_CONFREGA 0x00
#define HMC5883L_CONFREGB 0x01
#define HMC5883L_MODEREG 0x02
#define HMC5883L_DATAREGBEGIN 0x03

//setup measurement mode
#define HMC5883L_MEASURECONTINOUS 0x00
#define HMC5883L_MEASURESINGLESHOT 0x01
#define HMC5883L_MEASUREIDLE 0x03
#define HMC5883L_MEASUREMODE HMC5883L_MEASURECONTINOUS

//setup scale
#define HMC5883L_SCALE088 1 //0.88
#define HMC5883L_SCALE13 2 //1.3
#define HMC5883L_SCALE19 3 //1.9
#define HMC5883L_SCALE25 4 //2.5
#define HMC5883L_SCALE40 5 //4.0
#define HMC5883L_SCALE47 6 //4.7
#define HMC5883L_SCALE56 7 //5.6
#define HMC5883L_SCALE81 8 //8.1
#define HMC5883L_SCALE HMC5883L_SCALE13

void hmc5883l_init();
void hmc5883l_updatedata(uint8_t *buf);

#endif /* HMC5883L_H_ */