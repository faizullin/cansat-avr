/*
 * l3g4200d.c
 *
 * Created: 02.06.2014 11:08:47
 * Author: Roman
 */ 


#include <stdlib.h>
#include <avr/io.h>
#include <util/delay.h>
#include "i2cmaster.h"

#include "l3g4200d.h"

void l3g4200d_updatedata(uint8_t *buffer) {
	i2c_start(L3G4200D_ADDR | I2C_WRITE);
	i2c_write(L3G4200D_OUT_X_L | (1 << 7));
	i2c_rep_start(L3G4200D_ADDR | I2C_READ);
	
	*buffer = i2c_readAck();
	*(buffer + 1) = i2c_readAck();
	*(buffer + 2) = i2c_readAck();
	*(buffer + 3) = i2c_readAck();
	*(buffer + 4) = i2c_readAck();
	*(buffer + 5) = i2c_readNak();
	
	i2c_stop();
}

void l3g4200d_init() {
	//enable chip
	i2c_start(L3G4200D_ADDR | I2C_WRITE);
	i2c_write(L3G4200D_CTRL_REG1);
	i2c_write(0x0F); //0x0F = 0b00001111, normal power mode, all axes enabled
	i2c_stop();
	//set range
	i2c_start(L3G4200D_ADDR | I2C_WRITE);
	i2c_write(L3G4200D_CTRL_REG4);
	i2c_write(L3G4200D_RANGE<<4);
	i2c_stop();
}