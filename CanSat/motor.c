﻿/*
 * motor.c
 *
 * Created: 28.05.2014 23:03:51
 * Author: Roman
 */ 

#include "motor.h"

#define prescaler 64

void esc_init()
{
	DDRB |= (1<<PB1);
	TCCR1A |= (1<<COM1A1)|(1<<COM1B1)|(1<<WGM11); //non-inverted pwm
	TCCR1B |= (1<<WGM12)|(1<<WGM11)|(1<<WGM10)|(1<<CS11); //fast-pwm, prescaler 8
	ICR1=F_CPU/(50*8);//50hz output 20 000
	OCR1A=1000; //OCR1A = required time / period 
}

void setForce(double percent)
{
	OCR1A=(int)((MAX_IMPULSE*percent)/100);
}