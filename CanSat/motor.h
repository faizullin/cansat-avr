﻿/*
 * motor.h
 *
 * Created: 28.05.2014 23:03:09
 * Author: Roman
 */ 


#ifndef MOTOR_H_
#define MOTOR_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#define MAX_IMPULSE 2000 //microsec

void esc_init();
void setForce(double percent);

#endif /* MOTOR_H_ */