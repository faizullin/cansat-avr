﻿/*
 * ms5611.c
 *
 * Created: 28.05.2014 22:19:40
 * Author: Roman
 */ 

#include "ms5611.h"
#include "i2cmaster.h"

const uint16_t C1 = 51387;
const uint16_t C2 = 49825;
const uint16_t C3 = 31718;
const uint16_t C4 = 27787;
const uint16_t C5 = 31894;
const uint16_t C6 = 28075;

volatile static int32_t dt;
volatile static int32_t temp;
volatile static uint32_t temperature_raw;
volatile static uint32_t pressure_raw;

union u temperature,pressure;

void ms5611_requestTemperature()
{
	i2c_start_wait(MS5611_ADDR+I2C_WRITE);
	i2c_write(MS5611_TEMPERATURE);
	i2c_stop();
}

void ms5611_updateRawTemperature()
{
	i2c_start_wait(MS5611_ADDR+I2C_WRITE);
	i2c_write(0x00);
	i2c_stop();
	
	i2c_start_wait(MS5611_ADDR+I2C_READ);
	temperature_raw = ((uint32_t)i2c_readAck()<<16);
	temperature_raw |= ((uint32_t)i2c_readAck()<<8);
	temperature_raw |= (uint32_t)i2c_readNak();
	i2c_stop();
}

void ms5611_requestPressure()
{
	i2c_start(MS5611_ADDR|I2C_WRITE);
	i2c_write(MS5611_PRESSURE);
	i2c_stop();
}

void ms5611_updateRawPressure()
{
	i2c_start_wait(MS5611_ADDR+I2C_WRITE);
	i2c_write(0x00);
	i2c_stop();
	
	i2c_start(MS5611_ADDR|I2C_READ);
	pressure_raw = ((uint32_t)i2c_readAck()<<16);
	pressure_raw |= ((uint32_t)i2c_readAck()<<8);
	pressure_raw |= (uint32_t)i2c_readNak();
	i2c_stop();
}

void ms5611_calculatePressure()
{
  int64_t off = (((int64_t)C2) << 16) + (((int64_t)C4 * (int64_t)dt) >> 7);
  int64_t sens = (((int64_t)C1) << 15) + (((int64_t)C3 * (int64_t)dt) >> 8);
  int64_t off2;
  int64_t sens2;
  
  if (temp<2000){
	  int64_t temp64 = (int64_t)temp;
	  int64_t a = 5*(temp64-2000)*(temp64-2000);
	  off2 = (a>>1);
	  sens2 = (off2>>1);
	  off -= off2;
	  sens -= sens2;
  }
  
  pressure.num32 = (((((int64_t)pressure_raw * sens) >> 21) - off) >> 15);
}


void ms5611_calculateTemperature()
{
	dt = (int32_t)temperature_raw - (((int32_t)C5) << 8);
	int32_t t2;
	
	int32_t temp = (int16_t)(2000 + (int32_t)(((int64_t)dt * (int64_t)C6) >> 23));
	if (temp<2000){
		t2 = (dt*dt>>31);
		temp -= t2;
	}
	
	temperature.num32 = temp;
}

void ms5611_temperatureToBuffer(uint8_t *buf)
{
	*buf = temperature.bytes.low;
	*(buf + 1) = temperature.bytes.med;
	*(buf + 2) = temperature.bytes.hi;
	*(buf + 3) = temperature.bytes.vhi;
}

void ms5611_pressureToBuffer(uint8_t *buf)
{
	*buf = pressure.bytes.low;
	*(buf + 1) = pressure.bytes.med;
	*(buf + 2) = pressure.bytes.hi;
	*(buf + 3) = pressure.bytes.vhi;
}