﻿/*
 * ms5611.h
 *
 * Created: 28.05.2014 22:12:08
 * Author: Roman
 */ 


#ifndef MS5611_H_
#define MS5611_H_

#include <avr/io.h>

#define MS5611_ADDR (0x76<<1)
#define MS5611_PRESSURE 0x48 //OSR 4096
#define MS5611_TEMPERATURE 0x58 //OSR 4096

union u
{
	int32_t num32;

	struct
	{
		uint8_t vhi;
		uint8_t hi;
		uint8_t med;
		uint8_t low;

	} bytes;

};

extern union u temperature,pressure;

void ms5611_init();
void ms5611_requestTemperature();
void ms5611_requestPressure();
void ms5611_updateRawTemperature();
void ms5611_updateRawPressure();
void ms5611_calculateTemperature();
void ms5611_calculatePressure();

void ms5611_temperatureToBuffer(uint8_t *buf);
void ms5611_pressureToBuffer(uint8_t *buf);

#endif /* MS5611_H_ */