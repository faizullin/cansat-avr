/*
 * sim908.c
 *
 * Created: 02.07.2014 4:55:32
 * Author: Roman
 */ 

#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "global.h"

#define start_index 2

enum sim908cmdt {
	getgps,
	sendsms
	} sim908cmd;

const unsigned char gpsinfcmd[] = "AT+CGPSINF=0\r";
unsigned char pos_mainbuf=0;
static unsigned char pos = 0;

void uart0_init()
{
	UBRR0H = 0;
	UBRR0L = 12;// baudrate 38400
		
	UCSR0B |= (1<<TXEN0)|(1<<RXEN0);
	UCSR0C |= (3<<UCSZ00);
}

uint8_t comma_counter = 0;

ISR(USART0_RX_vect)
{
	if (sim908cmd == getgps)
	{
		char c = UDR0;
		if (c==0x2C) {
			comma_counter++;
		}
		
		else if ((comma_counter < 3) && (comma_counter > 1)) {
			data_buf[pos_mainbuf++ + start_index] = c;
		}
		
		else if (comma_counter == 3) {
			UCSR0B &= ~(1<<RXCIE0);
			pos_mainbuf = 0;
			comma_counter = 0;
		}
	}
}

ISR(USART0_UDRE_vect)
{
	if (sim908cmd == getgps){
		if (gpsinfcmd[pos])
			UDR0 = gpsinfcmd[pos++];
		else {
			UCSR0B &= ~(1<<UDRIE0);
			UCSR0B |= (1<<RXCIE0);
			pos = 0;	
		}			
	}
	else if (sim908cmd == sendsms){
		//coming soon
	}
	else {
		// coming soon
	}

}

void sim908_powerOn()
{
	DDRA |= (1<<PA5);
	PORTA |= (1<<PA5);
	_delay_ms(100);
	PORTA &= ~(1<<PA5);
	_delay_ms(1200);
	PORTA |= (1<<PA5);
}

void sim908_config()
{
	uart0_init();
	
	for (int i=0;i<5;i++) {
		uart0_puts("AT\r");
		_delay_ms(50);
	}
	
	uart0_puts("AT+CGPSPWR=1\r");
	_delay_ms(50);
	
	uart0_puts("AT+CGPSRT=0\r");
	_delay_ms(50);
}

void sim908_getGPS()
{
	sim908cmd = getgps;
	UCSR0B |= (1<<UDRIE0);
	pos = 0;
	pos_mainbuf = 0;
}

void uart0_putc(unsigned char c)
{
	while (!(UCSR0A & (1<<UDRE0)));
	UDR0 = c;
}

void uart0_puts(unsigned char *s)
{
	while (*s){
		uart0_putc(*s++);
	}
}