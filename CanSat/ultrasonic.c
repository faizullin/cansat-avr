/*
 * ultrasonic.c
 *
 * Created: 21.06.2014 22:16:36
 *  Author: Roman
 */ 


/* It's better to comment all this stuff now. */



/*
#include "ultrasonic.h"
#include "avr/interrupt.h"

static const uint32_t max_ticks = (uint32_t)MAX_RESP_TIME_MS * INSTR_PER_MS;
static volatile long result = 0;
static volatile unsigned char up = 0;
static volatile unsigned char running = 0;
static volatile uint32_t timerCounter = 0;
static volatile uint16_t ovf_counter_trigger = 0;

uint16_t getDistance()
{
	return result;
}

void trig()
{
	//HOW TO CORRECT?!?!?!?!?!?!"?!
	 PORTB &= (0<<TrigPin); // clear to zero for 1 us
	 _delay_us(1);
	 PORTB |= (1<<TrigPin); // set high for 10us
	 running = 1;  // sonar launched
	 _delay_us(10);
	 PORTB &= (0<<TrigPin);
}

void ultrasonic_init()
{
	DDRB |= (1<<TrigPin);
	MCUCR |= (0 << ISC11) | (1 << ISC10); // enable interrupt on any(rising/droping) edge
	GICR |= (1 << INT1);  
	
	TCCR2 = (0<<CS22)|(0<<CS21)|(1<<CS20); // select internal clock with no prescaling
	TCNT2 = 0; // reset counter to zero
	TIMSK |= 1<<TOIE2; // enable timer interrupt
}

ISR(TIMER2_OVF_vect)
{
	if (up) {       // voltage rise was detected previously
		timerCounter++; // count the number of overflows
		// dont wait too long for the sonar end response, stop if time for measuring the distance exceeded limits
		uint32_t ticks = timerCounter * 256 + TCNT0;
		if (ticks > max_ticks) {
			// timeout
			up = 0;          // stop counting timer values
			running = 0; // ultrasound scan done
			result = -1; // show that measurement failed with a timeout (could return max distance here if needed)
		}
	}
}

ISR(INT1_vect)
{
	if (running) { //accept interrupts only when sonar was started
		if (up == 0) { // voltage rise, start time measurement
			up = 1;
			timerCounter = 0;
			TCNT0 = 0; // reset timer counter
			} else {
			// voltage drop, stop time measurement
			up = 0;
			// convert from time to distance(millimeters): d = [ time_s * 340m/s ] / 2 = time_us/58
			result = (timerCounter * 256 + TCNT0) / 58;
			running = 0;
		}
	}
}*/