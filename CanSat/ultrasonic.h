/*
 * ultrasonic.h
 *
 * Created: 21.06.2014 21:49:22
 *  Author: Roman
 */ 


#ifndef ULTRASONIC_H_
#define ULTRASONIC_H_

#define SoundSpeedCoef 343/2 //if temperature = 20C
#define TrigPin PB1
#define INSTR_PER_MS 8000                // instructions per millisecond (depends on MCU clock, 12MHz current)
#define MAX_RESP_TIME_MS 200      // timeout - max time to wait for low voltage drop (higher value increases measuring distance at the price of slower sampling)

void ultrasonic_init();
void trig();
uint16_t getDistance();

#endif /* ULTRASONIC_H_ */