# CanSat 2014 ГАГАРИН-М team firmware #

### Used hardware ###

* Atmel ATMega128
* MS5611 pressure and temperature sensor
* GY-80 position sensors kit
* Hobbyking ESC
* RXQ2 radio transmitter
* Noname camera
* DIY printed circuit boards

Our microsatellite photo:

![bc8c78f02f2aba05d7b6fda64599b991.jpg](https://bitbucket.org/repo/XLqnrG/images/2547675806-bc8c78f02f2aba05d7b6fda64599b991.jpg)